.. LoopDetect documentation master file, created by
   sphinx-quickstart on Wed Oct 14 23:22:11 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LoopDetect's documentation!
======================================

LoopDetect provides Python functions to determine all feedback loops of
an ordinary differential equation (ODE) system. The example workflow description shows how to use the provided functions, the index provides an overview over all functions.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   workflow

* :ref:`genindex`
* :ref:`search`

.. automodule:: core
	:members:

.. automodule:: examples
	:members:

